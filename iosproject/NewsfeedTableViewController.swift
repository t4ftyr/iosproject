//
//  NewsfeedTableViewController.swift
//  iosproject
//
//  Created by YEUNG Kwok Yee on 5/1/2020.
//  Copyright © 2020 YEUNG Kwok Yee. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAnalytics
import Firebase

class NewsfeedTableViewController: UITableViewController {
    
    var posts = [Post]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // download posts
        Database.database().reference().child("posts").observe(.childAdded) { (snapshot) in
            // snapshot is now a dictionary
            let newPost = Post(snapshot: snapshot)
            DispatchQueue.main.async {
                self.posts.insert(newPost, at: 0)
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView.insertRows(at: [indexPath], with: .top)
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCell", for: indexPath) as! PhotoTableViewCell
        let post = self.posts[indexPath.row]
        
        cell.post = post

        return cell
    }
}
