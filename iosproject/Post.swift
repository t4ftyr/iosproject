//
//  Post.swift
//  iosproject
//
//  Created by YEUNG Kwok Yee on 5/1/2020.
//  Copyright © 2020 YEUNG Kwok Yee. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SwiftyJSON
import FirebaseDatabase
import FirebaseStorage

class Post {
    private var image: UIImage!
    var caption: String!
    var downloadURL: String?
    
    init(image: UIImage, caption: String) {
        self.image = image
        self.caption = caption
    }
    
    init(snapshot: DataSnapshot) {
        let json = JSON(snapshot.value!)
        self.caption = json["caption"].stringValue
        self.downloadURL = json["imageDownloadURL"].string
    }
    
    func save(viewController: UIViewController) {
        
        let newPostRef = Database.database().reference().child("posts").childByAutoId()
        let newPostKey : String = newPostRef.key ?? "postKeyImage_" + randomString(length: 10)
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium.self
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        viewController.present(alert, animated: true, completion: nil)
        // 1. save image
        if let imageData = image.jpegData(compressionQuality: 0.5) {
            let storage = Storage.storage().reference().child("images/\(String(describing: newPostKey))")
            
            storage.putData(imageData).observe(.success, handler: { (snapshot) in
                storage.downloadURL(completion: {url, error in
                    if (error == nil) {
                        if(url != nil){
                            self.downloadURL = url!.absoluteString
                            let postDictionary = [
                                "imageDownloadURL" : self.downloadURL,
                                "caption" : self.caption
                            ]
                                newPostRef.setValue(postDictionary)
                            print("finished")

                            viewController.dismiss(animated: false, completion: nil)
                            viewController.dismiss(animated: true, completion: nil)
                            
                        }else{
                            
                            viewController.dismiss(animated: false, completion: nil)
                            print("url nill");
                        }
                } else {

                        viewController.dismiss(animated: false, completion: nil)
                        print(error as Any);
                }
                    
            })
            })
        }
    }
    func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0..<length).map{ _ in letters.randomElement()! })
    }
}
