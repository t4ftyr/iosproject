//
//  ViewController.swift
//  iosproject
//
//  Created by YEUNG Kwok Yee on 5/1/2020.
//  Copyright © 2020 YEUNG Kwok Yee. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController
{

    @IBAction func loginAnonymouslyDidTap(_ sender: Any)
    {
        Auth.auth().signInAnonymously { (user, error) in
            if error == nil {
                // successfully sign in anonymously
                self.performSegue(withIdentifier: "ShowNewsfeed", sender: nil)
            }
        }
    }
    
    @IBAction func facebookSignUpDidTap(_ sender: Any)
    {
        
    }
    
}

